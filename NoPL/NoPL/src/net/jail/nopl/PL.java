package net.jail.nopl;

import org.bukkit.Server;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import net.md_5.bungee.api.ChatColor;

public class PL extends JavaPlugin implements Listener{
	Server s = this.getServer();
	
	public void onEnable(){
		s.getPluginManager().registerEvents(this, this);
	}
	public void onDisable(){
		
	}
	
	@EventHandler
	public void onCommandPreProcess(PlayerCommandPreprocessEvent e){
		if(e.getMessage().contains("/?") || e.getMessage().equals("/pl") || e.getMessage().contains("/plugins") || e.getMessage().contains("/bukkit:")|| e.getMessage().contains("/minecraft:")){
			e.getPlayer().sendMessage(ChatColor.GOLD + "This command has been blocked on this server!");
			e.setCancelled(true);
		}
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		StringBuilder sb = new StringBuilder();
		if(cmd.getName().equalsIgnoreCase("nplug")){
			if(sender.hasPermission("nopl.list")){
			for (Plugin pl : s.getPluginManager().getPlugins()){
				
				sb.append(ChatColor.GOLD + pl.getName() + ", ");
			}
			sender.sendMessage(ChatColor.GREEN + "Plugins: " + sb.toString());
		}else{
			sender.sendMessage("You do not have permission to use this plugin");
		}
		}
		return false;
	}
}
